import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    # db = os.environ.get("DB", None) or os.environ.get("database", None)
    # username = os.environ.get("USER", None) or os.environ.get("username", None)
    # password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    # hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)

    # TODO: change back to getting from env vars
    db = 'testdb'
    username = 'root'
    password = 'testpass123!@#'
    hostname = '35.192.13.113'

    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movie(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating TEXT, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) VALUES ('test_year', 'test_title', 'test_director', 'actor', 'release_date', 'rating')")
    # cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT * FROM movie")
    entries = [dict(title=row[1]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None




#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request to add movie.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    print(year, title, director, actor, release_date, rating)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        # cur.execute("INSERT INTO movie () values ('" + msg + "')")
        cur.execute("INSERT INTO movie (year, title, director, actor, release_date, rating) VALUES ('" + year + "', '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', '" + rating + "')")
        cnx.commit()
    except:
        success_message = ("movie %s successfully inserted" % s)
        return render_template('index.html', msg=success_message)
    else:
        return render_template('index.html')


    return render_template('index.html')


@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request to update movie.")
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']
    print(year, title, director, actor, release_date, rating)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cmd = "update movie set title='"+ title +"', director='"+ director +"', actor='"+ actor +"', release_date='"+ release_date +"', rating='"+ rating +"', year='"+ year +"' where title='"+ title +"'"
    cur.execute(cmd)
    cnx.commit()
    return render_template('index.html')


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request to delete movie.")
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cmd = "delete from movie where title='"+ title +"'"
    cur.execute(cmd)
    cnx.commit()
    return render_template('index.html')

@app.route('/search_movie', methods=['GET'])
def search_actor():
    print("Received request to search actor.")
    print('\n\n\n\n')
    actor = request.args['search_actor']
    print(actor)

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cmd = "select * from movie where actor='"+ actor +"'"
    cur.execute(cmd)
    # cnx.commit()
    entries = [dict(year=row[1], title=row[2], actor=row[4]) for row in cur.fetchall()]
    return render_template('index.html', entries=entries)


@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request to highest rating.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cmd = "SELECT MAX(rating) FROM movie"
    cur.execute(cmd)
    # cnx.commit()
    print('\n\n\n\n')
    entries = [dict(rating=row[0]) for row in cur.fetchall()]
    rating = entries[0]['rating']


    cur = cnx.cursor()
    cmd = "SELECT * FROM movie where rating='"+ rating +"'"
    cur.execute(cmd)
    entries = [dict(year=row[1], title=row[2], director=row[3], actor=row[4], release_date=row[5], rating=row[6]) for row in cur.fetchall()]


    return render_template('index.html', highest_entries=entries)




@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request to lowest rating.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cmd = "SELECT MIN(rating) FROM movie"
    cur.execute(cmd)
    # cnx.commit()
    print('\n\n\n\n')
    entries = [dict(rating=row[0]) for row in cur.fetchall()]
    rating = entries[0]['rating']


    cur = cnx.cursor()
    cmd = "SELECT * FROM movie where rating='"+ rating +"'"
    cur.execute(cmd)
    entries = [dict(year=row[1], title=row[2], director=row[3], actor=row[4], release_date=row[5], rating=row[6]) for row in cur.fetchall()]


    return render_template('index.html', lowest_entries=entries)



#  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~





@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    # entries = query_data()
    # print("Entries: %s" % entries)
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
