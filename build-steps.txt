echo "This is a build step"

cd CICD/greetings
docker build -t gcr.io/assignment-4-cloud-236219/greetings .
gcloud docker -- push gcr.io/assignment-4-cloud-236219/greetings

echo "SUCCESS"
